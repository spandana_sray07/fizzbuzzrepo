﻿
namespace FizzBuzz.Controllers
{
    using System.Threading.Tasks;
    using FizzBuzz.CQRS;
    using FizzBuzz.Model;
    using MediatR;
    using Microsoft.AspNetCore.Mvc;
    using X.PagedList;

    public class FizzBuzzController : Controller
    {
        private readonly IMediator _mediator;

        public FizzBuzzController(IMediator mediator)
        {
            _mediator = mediator;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Index(FizzBuzzViewModel model)
        {
            if (ModelState.IsValid)
            {
                var fizzbuzz = new ProcessFizzBuzz.Context(model.Number ?? 0);
                var response = await _mediator.Send(fizzbuzz);                
                model.Results = response.Results.ToPagedList(1, 20);
                return View(model);
            }
            return View();
        }

        public async Task<IActionResult> FizzBuzz(FizzBuzzViewModel model)
        {
            var fizzbuzz = new ProcessFizzBuzz.Context(model.Number ?? 0);
            var pagenumber = model.Page ?? 1;
            var response = await _mediator.Send(fizzbuzz);            
            model.Results = response.Results.ToPagedList(pagenumber, 20);
            return View("Index", model);
        }
    }
}
