﻿namespace FizzBuzz.Model
{
    using X.PagedList;

    public class ProcessFizzBuzzResponse
    {        
        public IPagedList<string> Results { get; set; }        
    }
}