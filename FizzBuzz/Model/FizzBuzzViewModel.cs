﻿namespace FizzBuzz.Model
{
    using System.ComponentModel.DataAnnotations;
    using X.PagedList;

    public class FizzBuzzViewModel
    {
        [Required]
        [Range(1, 1000)]
        public int? Number { get; set; }
        public IPagedList<string> Results { get; set; }
        public int? Page { get; set; }
    }
}
