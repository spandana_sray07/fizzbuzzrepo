﻿namespace FizzBuzz.Constant
{

    public static class Constants
    {
        public const string Fizz = "Fizz";
        public const string Buzz = "Buzz";
        public const string Wizz = "Wizz";
        public const string Wuzz = "Wuzz";
        public const string WizzWuzz = "Wizz Wuzz";
        public const string FizzBuzz = "Fizz Buzz";
    }
}
