﻿namespace FizzBuzz.CQRS
{
    using System.Threading;
    using System.Threading.Tasks;
    using FizzBuzz.Model;
    using FizzBuzz.Services.Contract;
    using MediatR;
    using X.PagedList;

    public class ProcessFizzBuzz : IRequestHandler<ProcessFizzBuzz.Context, ProcessFizzBuzzResponse>
    {
        public struct Context : IRequest<ProcessFizzBuzzResponse>
        {
            public int Number { get; set; }

            public Context(int number)
            {
                this.Number = number;
            }
        }

        private readonly IFizzBuzz iFizzBuzz;

        public ProcessFizzBuzz(IFizzBuzz data)
        {
            iFizzBuzz = data;
        }       

        public async Task<ProcessFizzBuzzResponse> Handle(Context request, CancellationToken cancellationToken)
        {
            return new ProcessFizzBuzzResponse
            {
                Results = await Task.FromResult(iFizzBuzz.FizzBuzzResult(request.Number).ToPagedList())
            };
        }
    }
    
}
