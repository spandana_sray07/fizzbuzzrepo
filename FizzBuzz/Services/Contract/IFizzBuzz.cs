﻿namespace FizzBuzz.Services.Contract
{
    using System.Collections.Generic;

    public interface IFizzBuzz
    {
        List<string> FizzBuzzResult(int number);
    }
}
