﻿namespace FizzBuzz.Service
{
    using System.Collections.Generic;
    using System.Linq;
    using Rules.Contract;
    using Services.Contract;

    public class FizzBuzz : IFizzBuzz
    {        
        private readonly IEnumerable<IRule> rules;

        public FizzBuzz(IEnumerable<IRule> rules) => this.rules = rules;

        public List<string> FizzBuzzResult(int number)
        {
            var list = new List<string>();
            IEnumerable<int> numbers = Enumerable.Range(1, number).Select(n => n);
            foreach (var num in numbers)
            {
                list.Add(GetFizzBuzz(num));
            }
            return list;
        }

        private string GetFizzBuzz(int number)
        {
            string result = string.Empty;
            foreach(var rule in rules)
            {
                if (rule.IsRuleApplicable(number))
                {
                    return rule.ApplyRule();
                }                
            }
            return number.ToString();
        }
    }
}
