﻿namespace FizzBuzz.Rules.Contract
{

    public interface IDayProvider
    {
        bool IsSpecificFizzBuzzDay();
    }
}
