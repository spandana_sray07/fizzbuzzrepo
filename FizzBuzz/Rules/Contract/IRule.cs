﻿namespace FizzBuzz.Rules.Contract
{

    public interface IRule
    {
        bool IsRuleApplicable(int number);
        string ApplyRule();
    }
}
