﻿namespace FizzBuzz.Rules
{
    using FizzBuzz.Constant;
    using FizzBuzz.Rules.Contract;

    public class BuzzRule:IRule
    {
        private readonly IDayProvider iDayProvider;

        public BuzzRule(IDayProvider dayProvider)
        {
            iDayProvider = dayProvider;
        }

        public string ApplyRule()
        {
            return iDayProvider.IsSpecificFizzBuzzDay() ? Constants.Wuzz : Constants.Buzz;
        }

        public bool IsRuleApplicable(int number) => number % 5 == 0;     
    }
}
