﻿namespace FizzBuzz.Rules
{
    using FizzBuzz.Constant;
    using FizzBuzz.Rules.Contract;

    public class FizzBuzzRule : IRule
    {

        private readonly IDayProvider iDayProvider;

        public FizzBuzzRule(IDayProvider dayProvider)
        {
            iDayProvider = dayProvider;
        }

        public string ApplyRule()
        {
            return iDayProvider.IsSpecificFizzBuzzDay() ? Constants.WizzWuzz : Constants.FizzBuzz;
        }

        public bool IsRuleApplicable(int number) => number % 3 == 0 && number % 5 == 0;        
    }
}
