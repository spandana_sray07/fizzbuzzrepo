﻿namespace FizzBuzz.Rules
{
    using FizzBuzz.Constant;
    using FizzBuzz.Rules.Contract;

    public class FizzRule : IRule
    {

        private readonly IDayProvider iDayProvider;

        public FizzRule(IDayProvider dayProvider)
        {
            iDayProvider = dayProvider;
        }

        public string ApplyRule()
        {
            return iDayProvider.IsSpecificFizzBuzzDay() ? Constants.Wizz : Constants.Fizz;
        }

        public bool IsRuleApplicable(int number) => number % 3 == 0;
    }
}
