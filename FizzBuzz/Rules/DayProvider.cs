﻿namespace FizzBuzz.Rules.Services
{
    using System;
    using FizzBuzz.Rules.Contract;
    using Microsoft.Extensions.Options;

    public class DayProvider : IDayProvider
    {
        private readonly DayConfigOption config;

        public DayProvider(IOptions<DayConfigOption> options)
        {
            config = options.Value;
        }

        public bool IsSpecificFizzBuzzDay()
        {
            if (DateTime.Today.DayOfWeek.ToString() == config.SpecificFizzBuzzDay)
            {
                return true;
            }
            return false;
        }
    }
}
