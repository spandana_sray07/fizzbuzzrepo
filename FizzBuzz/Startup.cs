namespace FizzBuzz
{
    using System.Reflection;
    using FizzBuzz.Rules;
    using FizzBuzz.Rules.Contract;
    using FizzBuzz.Rules.Services;
    using FizzBuzz.Services.Contract;
    using MediatR;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;

    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<DayConfigOption>(Configuration.GetSection("DayConfigOption"));
            services.AddControllers();

            services.AddScoped<IDayProvider, DayProvider>();
            services.AddScoped<IFizzBuzz, FizzBuzz.Service.FizzBuzz>();
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddScoped<IRule, FizzBuzzRule>();
            services.AddScoped<IRule, FizzRule>();
            services.AddScoped<IRule, BuzzRule>();

            services.AddMvc(options => options.EnableEndpointRouting = false);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseStaticFiles();
            app.UseMvc(routes =>
            {                
                routes.MapRoute(
                    name: "default",
                    template: "{controller=FizzBuzz}/{action=Index}/{id?}"
                    );
            });
        }
    }
}
