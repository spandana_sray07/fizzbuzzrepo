﻿namespace FizzBuzz.UnitTest.Services
{
    using System.Collections.Generic;
    using FizzBuzz.Rules.Contract;
    using Moq;
    using Xunit;

    public class FizzBuzzTest
    {
        [Fact]

        public void FizzBuzzResult_Should_ReturnCorrectResponse()
        {
            //Arrange
            var expected = new List<string> { "1", "2", "Fizz", "4", "Buzz","Fizz", "7", "8", "Fizz", "Buzz","11", "Fizz", "13", "14", "Fizz Buzz" };
            var fizz = new Mock<IRule>();
            fizz.Setup(expression: m => m.IsRuleApplicable(It.IsAny<int>()))
                .Returns(valueFunction: (int number) => number % 3 == 0);
            fizz.Setup(expression: m => m.ApplyRule()).Returns("Fizz");

            var buzz = new Mock<IRule>();
            buzz.Setup(expression: m => m.IsRuleApplicable(It.IsAny<int>()))
                .Returns(valueFunction: (int number) => number % 5 == 0);
            buzz.Setup(expression: m => m.ApplyRule()).Returns("Buzz");

            var fizzbuzz = new Mock<IRule>();
            fizzbuzz.Setup(expression: m => m.IsRuleApplicable(It.IsAny<int>()))
                .Returns(valueFunction: (int number) => number % 3 == 0 && number % 5 == 0);
            fizzbuzz.Setup(expression: m => m.ApplyRule()).Returns("Fizz Buzz");

            List<IRule> ruleList = new List<IRule> { fizzbuzz.Object, fizz.Object, buzz.Object };

            var rules = new Mock<IEnumerable<IRule>>();
            rules.Setup(expression:m => m.GetEnumerator())
                .Returns(() => ruleList.GetEnumerator());
            var fizzbuzzService = new FizzBuzz.Service.FizzBuzz(rules.Object);

            //Act
            var response = fizzbuzzService.FizzBuzzResult(15);

            //Assert
            Assert.Equal(expected, response);
        }
    }
}
