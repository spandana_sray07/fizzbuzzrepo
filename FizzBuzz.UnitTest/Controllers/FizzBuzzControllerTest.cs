﻿namespace FizzBuzz.UnitTest.Controllers
{
    using System.Collections.Generic;
    using System.Threading;
    using FizzBuzz.Controllers;
    using FizzBuzz.CQRS;
    using FizzBuzz.Model;
    using MediatR;
    using Microsoft.AspNetCore.Mvc;
    using Moq;
    using X.PagedList;
    using Xunit;

    public class FizzBuzzControllerTest
    {
        [Fact]

        public void Index_ReturnsEmptyView_WithGetMethod()
        {

            //Arrange
            var mock = new Mock<IMediator>();            
            var controller = new FizzBuzzController(mock.Object);
            var result = controller.Index();
            var viewResult = (ViewResult)result;

            //Act
            var model = viewResult.ViewData.Model;
            
            //Assert
            Assert.Null(model);
        }

        [Fact]

        public async void Index_ReturnsAViewResult_WhenModelStateIsvalid()
        {

            //Arrange
            var mock = new Mock<IMediator>();            
            mock.Setup(mediatr => mediatr.Send(It.IsAny<ProcessFizzBuzz.Context>(),
                It.IsAny<CancellationToken>()))
                .ReturnsAsync(new ProcessFizzBuzzResponse { Results = new List<string> { "1", "2", "Fizz", "4", "Buzz" }.ToPagedList() });

            var controller = new FizzBuzzController(mock.Object);           
            
            var result = await controller.Index(new FizzBuzzViewModel { Number = 5 });
            var viewResult = (ViewResult)result;

            //Act
            var model = viewResult.ViewData.Model;
            mock.Verify(x => x.Send(It.IsAny<ProcessFizzBuzz.Context>(), It.IsAny<CancellationToken>()), Times.Once());
            
            //Assert
            Assert.NotNull(model);
        }

        [Fact]

        public async void Index_ReturnsAViewResult_WhenPageClicked()
        {

            //Arrange
            var mock = new Mock<IMediator>();
            mock.Setup(mediatr => mediatr.Send(It.IsAny<ProcessFizzBuzz.Context>(),
                It.IsAny<CancellationToken>()))
                .ReturnsAsync(new ProcessFizzBuzzResponse { Results = new List<string> { "1", "2", "Fizz", "4", "Buzz" }.ToPagedList() });

            var controller = new FizzBuzzController(mock.Object);

            var result = await controller.FizzBuzz(new FizzBuzzViewModel { Number = 5, Page = 2 });
            var viewResult = (ViewResult)result;

            //Act
            var model = viewResult.ViewData.Model;
            mock.Verify(x => x.Send(It.IsAny<ProcessFizzBuzz.Context>(), It.IsAny<CancellationToken>()), Times.Once());

            //Assert
            Assert.NotNull(model);
        }
    }
}
