﻿namespace FizzBuzz.UnitTest.Rules
{
    using System;
    using FizzBuzz.Rules.Services;
    using Microsoft.Extensions.Options;
    using Moq;
    using Xunit;

    public class DayProviderTest
    {
        [Fact]

        public void IsSpeicificFizzBuzzDay_CurrentDay_ShouldGiveTrue()
        {
            //Arrange
            var mock = new Mock<IOptions<DayConfigOption>>();
            mock.Setup(m => m.Value)
                .Returns(new DayConfigOption { SpecificFizzBuzzDay = DateTime.Today.DayOfWeek.ToString() });
            var dayRule = new DayProvider(mock.Object);

            //Act
            var response = dayRule.IsSpecificFizzBuzzDay();

            //Assert
            Assert.True(response);
        }
    }
}
