﻿namespace FizzBuzz.UnitTest.Rules
{
    using System;
    using FizzBuzz.Rules;
    using FizzBuzz.Rules.Contract;
    using Moq;
    using Xunit;

    public class BuzzRuleTest
    {
        [Fact]

        public void ApplyRule_ShouldGiveBuzz_WhenBuzzRule()
        {

            //Arrange
            var expected = "Buzz";
            var mock = new Mock<IDayProvider>();
            mock.Setup(expression: m => m.IsSpecificFizzBuzzDay()).Returns(false);
            var buzzRule = new BuzzRule(mock.Object);

            //Act
            var response=buzzRule.ApplyRule();

            //Assert
            Assert.Equal(expected, response);
        }

        [Fact]
        public void ApplyRule_ShouldGiveWuzz_WhenSpecificFizzBuzzDay()
        {

            //Arrange
            var expected = "Wuzz";
            var mock = new Mock<IDayProvider>();
            mock.Setup(expression: m => m.IsSpecificFizzBuzzDay()).Returns(true);
            var buzzRule = new BuzzRule(mock.Object);

            //Act
            var response = buzzRule.ApplyRule();

            //Assert
            Assert.Equal(expected, response);
        }

        [Fact]

        public void IsRuleApplicable_ShouldBeTrue_WhenDivisibleByFive()
        {

            //Arrange
            var mock = new Mock<IDayProvider>();
            var buzzRule = new BuzzRule(mock.Object);

            //Act
            var response = buzzRule.IsRuleApplicable(5);

            //Assert
            Assert.True(response);
        }

        [Fact]

        public void IsRuleApplicable_ShouldBeFalse_NotDivisibleByFive()
        {

            //Arrange
            var mock = new Mock<IDayProvider>();
            var buzzRule = new BuzzRule(mock.Object);

            //Act
            var response = buzzRule.IsRuleApplicable(7);

            //Assert
            Assert.False(response);
        }
    }
}
