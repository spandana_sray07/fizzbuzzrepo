﻿namespace FizzBuzz.UnitTest.Rules
{
    using FizzBuzz.Rules;
    using FizzBuzz.Rules.Contract;
    using Moq;
    using Xunit;

    public class FizzRuleTest
    {
        [Fact]
        public void ApplyRule_ShouldGiveFizz_WhenFizzRule()
        {
            //Arrange
            var expected = "Fizz";
            var mock = new Mock<IDayProvider>();
            mock.Setup(expression: m => m.IsSpecificFizzBuzzDay()).Returns(false);
            var fizzRule = new FizzRule(mock.Object);

            //Act
            var response = fizzRule.ApplyRule();

            //Assert
            Assert.Equal(expected, response);
        }

        [Fact]
        public void ApplyRule_ShouldGiveWizz_WhenSpecificFizzBuzzDay()
        {
            //Arrange
            var expected = "Wizz";
            var mock = new Mock<IDayProvider>();
            mock.Setup(expression: m => m.IsSpecificFizzBuzzDay()).Returns(true);
            var fizzRule = new FizzRule(mock.Object);

            //Act
            var response = fizzRule.ApplyRule();

            //Assert
            Assert.Equal(expected, response);
        }

        [Fact]

        public void IsRuleApplicable_ShouldBeTrue_WhenDivisibleByThree()
        {
            //Arrange
            var mock = new Mock<IDayProvider>();
            var fizzRule = new FizzRule(mock.Object);

            //Act
            var response = fizzRule.IsRuleApplicable(3);

            //Assert
            Assert.True(response);
        }

        [Fact]

        public void IsRuleApplicable_ShouldBeFalse_NotDivisibleByThree()
        {
            //Arrange
            var mock = new Mock<IDayProvider>();
            var fizzRule = new FizzRule(mock.Object);

            //Act
            var response = fizzRule.IsRuleApplicable(4);

            //Assert
            Assert.False(response);
        }
    }
}
