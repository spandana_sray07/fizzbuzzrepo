﻿namespace FizzBuzz.UnitTest.Rules
{
    using FizzBuzz.Rules;
    using FizzBuzz.Rules.Contract;
    using Moq;
    using Xunit;

    public class FizzBuzzRuleTest
    {
        [Fact]

        public void ApplyRule_ShouldGiveFizzBuzz_WhenFizzBuzzRule()
        {
            //Arrange
            var expected = "Fizz Buzz";
            var mock = new Mock<IDayProvider>();
            mock.Setup(expression: m => m.IsSpecificFizzBuzzDay()).Returns(false);
            var fizzbuzzRule = new FizzBuzzRule(mock.Object);

            //Act
            var response = fizzbuzzRule.ApplyRule();

            //Assert
            Assert.Equal(expected, response);
        }

        [Fact]
        public void ApplyRule_ShouldGiveWizzWuzz_WhenSpecificFizzBuzzDay()
        {
            //Arrange
            var expected = "Wizz Wuzz";
            var mock = new Mock<IDayProvider>();
            mock.Setup(expression: m => m.IsSpecificFizzBuzzDay()).Returns(true);
            var fizzbuzzRule = new FizzBuzzRule(mock.Object);

            //Act
            var response = fizzbuzzRule.ApplyRule();

            //Assert
            Assert.Equal(expected, response);
        }

        [Fact]

        public void IsRuleApplicable_ShouldBeTrue_WhenDivisibleByThreeAndFive()
        {
            //Arrange
            var mock = new Mock<IDayProvider>();
            var fizzRule = new FizzBuzzRule(mock.Object);

            //Act
            var response = fizzRule.IsRuleApplicable(15);

            //Assert
            Assert.True(response);
        }

        [Fact]

        public void IsRuleApplicable_ShouldBeFalse_NotDivisibleByThreeAndFive()
        {
            //Arrange
            var mock = new Mock<IDayProvider>();
            var fizzRule = new FizzBuzzRule(mock.Object);

            //Act
            var response = fizzRule.IsRuleApplicable(17);

            //Assert
            Assert.False(response);
        }
    }
}
