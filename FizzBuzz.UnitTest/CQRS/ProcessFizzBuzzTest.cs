﻿namespace FizzBuzz.UnitTest.CQRS
{
    using System.Collections.Generic;
    using System.Threading;
    using FizzBuzz.CQRS;
    using FizzBuzz.Services.Contract;
    using Moq;
    using Xunit;

    public class ProcessFizzBuzzTest
    {
        [Fact]
        public async void Handle_IRequest_ShouldGive_CorrectResult()
        {

            //Arrange
            var expected = new List<string> { "1", "2", "Fizz", "4", "Buzz" };
            var mock = new Mock<IFizzBuzz>();
            mock.Setup(m => m.FizzBuzzResult(It.IsAny<int>()))
                .Returns(new List<string> { "1", "2", "Fizz", "4", "Buzz" });

            var handler = new ProcessFizzBuzz(mock.Object);

            //Act
            var result = await handler.Handle(new ProcessFizzBuzz.Context { Number = 15 }, CancellationToken.None);
          
            //Assert
            Assert.Equal(expected, result.Results);
        }
    }
}
